#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
ICON="""AAABAAEAICAAAAEACACoCAAAFgAAACgAAAAgAAAAQAAAAAEACAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAD/ADIyMgAAFBQAAP//AADj4wAA/f0AAOrqAAApKQAWEhIANzc3AABhYQAAl5cA
FBQUAHJvbwAA+fkAABkZAADAwABGOzsAAN7eACYcHAAA29sAPi4uAAAICAAAFhYAAAoKAADKygCG
e3sAANPTAADv7wAAFxcAAOfnAAD7+wAA8vIAJSAgAAAVFQAA9fUAAC8vAAB/fwAXDg4AAPHxAADt
7QAA/v4AAJ2dAAC0tAAA7u4AAOLiAAD29gAA5eUAHRgYAADZ2QAArq4AAMvLAADg4AAA1dUACBoa
AADW1gAAKisAANTUACsfHwAA6OgAAO3sAAAAWwAAZmIAAAC2AABqZwAA6eQAAOjlAAAANAAAAMAA
AAsJAACPjAAA9PQAAIF6AAAAJgAAAOMAAABiAADIxAAAODMAAADKAAAAzwAAACkAAAADAAAATQAA
ANYAExISAAC4uAAAubkAAAAfAAAA/gD/AP8AAADCAAAAtQAAANIAAABaAACsqQAA19cAAJyZAAAB
NQAAAPsAAAE2AACCggATGxsAAN/fAAD4+AAAIhUAACATAADs7AABCgoAAMnJAAD8/AADCwsAACMj
AAACAgAA0tIAEg0NAAADAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/
//8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdAAhBAQEBAQEBgAj
AAAAAAAAAAAAAAAAAAAAAAAAcXIEBAQEBFpaBAQEBAQkAHMAAAAAAAAAAAAAAAAAcAQEBAQEBARa
WlpaBAQEBAQEBAAAAAAAAAAAAAAAAABtBAQEBAQEIFpaWloEBAQEBAQEbm8AAAAAAAAAAAAAZwQE
BAQEaGlaWlpaWlpqawQEBAQEBGwAAAAAAAAAAGAEBAQEBGFiY1paWlpaWgFkZQQEBAQEIGYAAAAA
AABWBAQEBARXWFkBWltAXF1aAQFeXwQEBAQELgAAAAAAADAEBAQEBE5PAVBRAAAAUlNUAQFGBAQE
BAQEVQAAAAAEBAQEBARDREVGRwQEBAQESElKS0xNBAQEBAQEAAAAOzwEBAQEPQA+PwQEBAQEBAQE
BAYAQEFCBAQEBAQAAAA3BAQEBAQ4AAAEBAQEBAQEBAQEBAQAOToEBAQEBAAAAAQEBAQEBDQYBgQE
BAQEBAQEBAQEBDUANgQEBAQEBAAABAQEBAQEMgAEBAQEBAQEBAQEBAQEBAAzBAQEBAQdAAAwBAQE
BAQEBAQEBAQEBAAABAQEBAQEBAQEBAQEBC8xAAUEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQE
LwAALgQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQvAAAuBAQEBAQEBAQEBAQEBAQEBAQEBAQE
BAQEBAQEBC8AACwEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQELQAAAAQEBAQEBAQEKSkqBAQE
BAQEBAQEBAQEBAQEBAQrAAAABAQEBAQEBA8ZAB0EBAQEBAQEAAAoBAQEBAQEBAAAAAATBAQEBAQE
BAQEBAQEBAQEBAQEBAQEBAQEBAQPJwAAAAAEBAQEBCMEBAQEBAAEBAAEBAQEJCUEBAQEBCYAAAAA
ABwEBAQEHQAEBAQABAQEBB4EHwQAIAQEBAQhIgAAAAABAAQEBAQEBAAXGAQEBAQEBAAZAAQEBAQE
BBobAAAAAAEAAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQVFgEBAAEBAQAAAAQEBAQEBAQEBAQEBAQE
BAQEBAQEExQAAAEBAQAAAAAAAAQEBAQEBAQEBAQEBAQEBAQEBBESAAAAAAEAAAAAAQEAAAQEBAQE
BAQEBAQEBAQEBAQPEAABAQAAAAAAAAEBAAAACgsEBAQEBAQEBAQEBAQMDQ4AAAABAAAAAAABAQAA
AAABAAIDBAUGBAQEBwQICQAAAAEAAAEBAQAAAAEAAAAAAQEAAAAAAAAAAAAAAAAAAAAAAQEAAAAB
Af/wD///gAH//gAAf/wAAD/4AAAf8AAAD+AAAAfAAAADwAAAA4AAAAGAAAABgAAAAQAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAACAAAABgAAAAYAAAAHAAAADwAAAA8AAAAPQAAABGAAADHwAAB7y
AAAn5wAAd89AA7HecA+c"""


if __name__ == "__main__":
    import wx
    from commons.iconic import MyIconTest
          
    class MyApp(wx.App):
        def OnInit(self):
            wx.InitAllImageHandlers()
            myframe = MyIconTest(None, icon=ICON)
            myframe.Show()
            return 1
        
    app = MyApp(0)
    app.MainLoop()