#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
mzcalc
8 april 2014
"""
#
import wx
from commons.iconic import Iconic
from commons.mass import get_mass, mass_group
from mzcalc_class import PeptMassFrame
#
try:
    from mzcalc_images import ICON
except ImportError:
    print('here')
    ICON = None
#
FRAME_TITLE = "  Protein m/z Calculator 3.1"
mass_group['cbmd'] = (57.05, 57.02146)
#
#
class CalcFrame(PeptMassFrame, Iconic):
    """"""
    calc_number = 1
    # 
    def __init__(self, *args, **kwds):

        PeptMassFrame.__init__(self, *args, **kwds)
        Iconic.__init__(self, icon=ICON)

        #self.calc = MiniCalc(self, style=wx.RAISED_BORDER)
        self.SetSize((320, 150))
        self.title = FRAME_TITLE
        self.SetTitle(self.title)
        self.Bind(wx.EVT_BUTTON, self.on_bt_clone, self.calc.bt_clone)
        self.Bind(wx.EVT_BUTTON, self.on_bt_calc, self.calc.bt_calc)
    # 
    def on_bt_calc(self, evt):
        """calculate peptide mass."""
        sequence = self.calc.selection = self.calc.tc.GetValue()
        #self.calc.write_selection()

        mods = sequence.count('K')
        charges = self.sp_charge.Value
        derivs = self.cbbx_mods.Value

        if self.calc.avg == 'avg':
            avg = 0
        else:
            avg = 1

        # itraq / tmt
        if derivs in mass_group:
            mass_tag = mass_group[derivs]
            mass_mods = (mods + 1) * mass_tag[avg]
        else:
            mass_mods = 0

        # carbamidomethylated Cys
        if self.cbx_iaa.IsChecked():
            cys = sequence.count('C')
            mass_cys_cbmd = cys * mass_group['cbmd'][avg]
        else:
            mass_cys_cbmd = 0

        # oxidated methionines
        if self.cbx_mox.IsChecked():
            mets = sequence.count('M')
            mass_metox = mets * mass_group['Oxg'][avg]
        else:
            mass_metox = 0

        mass = get_mass(sequence, mode=self.calc.avg, water=self.calc.water)
        print(mass)

        mass += mass_metox + mass_mods + mass_cys_cbmd

        if charges > 0:
            self.calc.result = (mass +  mass_group['H'][avg] * charges) / charges
        else:
            self.calc.result = mass

        self.calc.write_result()
    #
    def on_bt_clone(self, evt):
        """"""
        CalcFrame.calc_number += 1
        titulo = "%s  #%i" % (self.title, CalcFrame.calc_number)
        calculadora = CalcFrame(None, -1, size=(250, 100))
        calculadora.SetTitle(titulo)
        posx, posy = self.GetPosition()
        szx, szy = self.GetSize()
        winsize = wx.GetDisplaySize()
        desvio = 10 * (self.calc_number - 2)
        #
        if (posx+szx+250) > winsize.GetWidth():         # se sale por la derecha
            if posy+szy+100 > winsize.GetHeight():      # se sale por debajo
                # go left
                newpos = (posx - 250 - desvio, posy + szy - 100 - desvio)
            else:
                # go down
                newpos = (posx + szx - 250 + desvio, posy + szy + desvio) 
        else:
            # vete a la derecha
            newpos = (posx + szx + desvio, posy + szy - 100 + desvio)  
        
        calculadora.SetPosition(newpos)
        calculadora.Show()
#
#
#
if __name__ == "__main__":
            
    MyApp = wx.App()
    minicalculadora = CalcFrame(None)
    minicalculadora.Show()
    MyApp.MainLoop()
