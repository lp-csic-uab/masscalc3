
---

**WARNING!**: This is the *Old* source-code repository for MassCalc3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/masscalc3/) located at https://sourceforge.net/p/lp-csic-uab/masscalc3/**  

---  
  
  
# MassCalc3 program

An easy to use mass calculator for proteomics (Python 3 version).

---

**WARNING!**: This is the *Old* source-code repository for MassCalc3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/masscalc3/) located at https://sourceforge.net/p/lp-csic-uab/masscalc3/**  

---  
  

