# exWx/setup.py
#
## python setup.py py2exe 
#
from distutils.core import setup
import py2exe

setup(
    windows = [
              {'script': "mcalc_class_mcalc_07.pyw",
               'icon_resources':[(0,'images/mcalc48.ico')],
               'dest_base' : "masscalc 0.7",    
               'version' : "0.7",
               'company_name' : "JoaquinAbian",
               'copyright' : "No Copyrights",
               'name' : "MassCalc" 
              }
              ],
            
    options = {
              'py2exe': {
                        #'packages' :    [],
                        #'includes':     [],
                        'excludes':     ['matplotlib', 'email', 'testing', 'fft',
                                         'Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
              },
    zipfile = None,
    data_files = [
                 ("", ["INSTALL.txt",    #Archivo de final de instalacion
                       "README.txt",     #archivo README y help de la aplicacion
                       "LICENCE.TXT"]    #Licence File (GPL v3)
                  )
                 ]
    )
